// const host = 'http://127.0.0.1/party/';
const host = 'http://192.168.5.87:8080/party/'
// const host = 'http://172.23.207.194:8080/party/'

//接口统一入口地址
const mobileIn = host + 'wechat/control/';

module.exports = {
  host,
  mobileIn
}